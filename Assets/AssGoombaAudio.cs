﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssGoombaAudio : MonoBehaviour {


    private AudioSource asource;
    public AudioClip[] clips;

    // Use this for initialization
    void Start()
    {
        asource = GetComponentInChildren<AudioSource>();
    }

    public void Hit()
    {
        asource.pitch = Random.Range(0.70f, 1.30f);
        asource.PlayOneShot(clips[Random.Range(1,3)], 0.8f);
    }

    public void Death()
    {
        asource.PlayOneShot(clips[0], 0.8f);
    }
}
