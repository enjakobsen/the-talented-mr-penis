﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Player : Unit
{

    private int MaxSP = 3;
    private int SP = 0;

    private Renderer main_rend;

    private Animator[] anim_sword;
    private Color original;
    private bool blinking;
    public bool invulnerable = false;


    public AudioClip SwingSound;



    override public void Awake()
    {
        base.Awake();
        main_rend = GetComponentInChildren<Renderer>();
        anim_sword = GetComponentsInChildren<Animator>();
        original = main_rend.material.color;


    }

    private void Start()
    {
        HP = MaxHP = 100;
    }


    public override void Damage(int dmgAmount)
    {
        if (!invulnerable)
        {
            HP -= dmgAmount;
            asource.PlayOneShot(HitSound);
            if (HP <= 0)
            {
                HP = 0;
                Deth();
            }
            BlinkRed(3f);
            FlagInvulnerable(2f);
        }
    }




    public void BlinkRed(float timer)
    {
        if (!blinking)
        {
            blinking = true;
            StartCoroutine(Blink(Color.red, 0.1f, timer));
        }
    }


    IEnumerator Blink(Color color, float delay, float timer)
    {
        float t = 0;
        while (blinking && t < timer)
        {
            main_rend.material.color = color;

            t += delay + Time.deltaTime;
            yield return new WaitForSeconds(delay);

            main_rend.material.color = original;
            t += delay + Time.deltaTime;
            yield return new WaitForSeconds(delay);

        }
        blinking = false;
        main_rend.material.color = original;
        yield return null;
    }



    public void FlagInvulnerable(float duration)
    {
        invulnerable = true;
        Invoke("FlagVulnerable", duration);
    }
    public void FlagVulnerable()
    {
        invulnerable = false;
    }

    public override void OnUnitDeth()
    {
        isDed = true;
    }


    private float FkeyCD = 0;
    private void Update()
    {


        if (Input.GetKeyDown(KeyCode.E))
        {
            anim.SetBool("Excited", true);

        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            anim.SetBool("Excited", false);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (FkeyCD <= 0)
            {
                anim_sword[1].SetTrigger("Swing");
                asource.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
                asource.PlayOneShot(SwingSound);
                FkeyCD = 0.5f;
                Collider[] cols = Physics.OverlapSphere(transform.position, 6f);
                List<Unit> HitUnits = new List<Unit>();
                foreach (Collider c in cols)
                {
                    Unit u = c.GetComponent<Unit>();
                    if (u != null)
                    {
                        if (!HitUnits.Contains(u) && c.gameObject.name != "Player")
                        {
                            if (Vector3.Angle(c.transform.position - transform.position, transform.forward) < 45f)
                            {
                                HitUnits.Add(u);
                            }
                        }
                    }
                }
                foreach (Unit u in HitUnits)
                {
                    u.KnockBack(transform.position);
                    u.Damage(20);
                }
            }
        }
        if (FkeyCD > 0)
        {
            FkeyCD -= Time.deltaTime;
        }
    }

}