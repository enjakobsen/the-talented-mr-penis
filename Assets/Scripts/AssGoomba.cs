﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssGoomba : Unit {

    
    private Player player;
    private Renderer rend;
    private float fadetime = 3f;
    private AssGoombaAudio Gaudio;

    

    public override void Awake()
    {
        base.Awake();
        player = FindObjectOfType<Player>();
        rend = gameObject.GetComponentInChildren<Renderer>();
        Gaudio = GetComponent<AssGoombaAudio>();
        MaxHP = HP = 40;
        KnockBackForce = 150f;
        KnockUpForce = 200f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isDed)
        {
            if (other.name == "Player")
            {
                anim.SetBool("Ded", true);
                Kill();
                isDed = true;
                Gaudio.Death();

            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!isDed)
        {
            if (other.gameObject.name == "Player")
            {
                player.Damage(30);
                player.KnockBack(other.contacts[0].point);

            }
        }
    }
    public override void Damage(int dmgAmount)
    {
        base.Damage(dmgAmount);
        Gaudio.Hit();
    }

    public override void OnUnitDeth()
    {
        
        base.OnUnitDeth();
        rend.material.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        kbticks = 0;
        rb.useGravity = false;
        rb.velocity = Vector3.zero;
        foreach (Collider col in GetComponentsInChildren<Collider>()) { col.enabled = false; }
        isDed = true;
        Gaudio.Death();
        //player.CheckSwingArea();

        





    }



    private void Update()
    {
        if (isDed)
        {
            if (fadetime > 0f)
            {
                fadetime -= Time.deltaTime;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }





}
