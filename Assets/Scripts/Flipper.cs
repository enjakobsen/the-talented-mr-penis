﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {



    private Rigidbody rb;
    private HingeJoint hj;
    private float targetpos;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = Mathf.Infinity;
        hj = GetComponent<HingeJoint>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.J))
        {
            targetpos = -30f;
        }
        else
        {
            targetpos = 15f;
        }
    }



    void FixedUpdate()
    {
        JointSpring mySpring = hj.spring;
        mySpring.targetPosition = targetpos;
        hj.spring = mySpring;



    }


        

	}

