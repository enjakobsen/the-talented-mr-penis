﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour {

    
    RectTransform rt;
    Text tx;
    Unit player;
    float originalwidth=360;
    float originalheight = 60;

	// Use this for initialization
	void Awake () {
        rt = GetComponent<RectTransform>();
        tx = transform.parent.GetComponentInChildren<Text>();
        player = FindObjectOfType<Player>();
        player.RegisterHPChanged( (hp, maxhp) => { OnUpdateHealthBar(hp,maxhp); } );
	}

    void OnUpdateHealthBar(int hp, int maxhp) {
        Debug.Log("OnUpdateHealthBar Called");
        float width = ((float)hp / (float)maxhp) * originalwidth;
        rt.sizeDelta = new Vector2(width, originalheight);
        tx.text = "HP: "+hp+"/"+maxhp;
	}
}
