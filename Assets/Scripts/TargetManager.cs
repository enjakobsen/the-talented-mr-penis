﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour {

    private RaycastHit hit;
    private Ray ray;
    public GameObject targetgo;
    public Unit targetu;

    private AudioSource fxaudio;
    public AudioClip onTarget;
    public AudioClip offTarget;

    private void Awake()
    {
        fxaudio = GetComponent<AudioSource>();
    }



    void Update () {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (targetgo == null)
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    TargetObject(hit.transform.gameObject);
                }
            }

        }
        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            TargetClear();
        }
    }




    public void TargetObject(GameObject t)
    {
        targetu = t.GetComponent<Unit>();
        if (targetu != null)
        {
            targetgo = t;
            fxaudio.PlayOneShot(onTarget);
        }
    }
    
    public void TargetClear()
    {
        targetgo = null;
        targetu = null;
        fxaudio.PlayOneShot(offTarget);
    }


}
