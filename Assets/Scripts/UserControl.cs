﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EmilsAssets
{
    [RequireComponent(typeof(PenisoidMove))]
    public class UserControl : MonoBehaviour
    {
        private PenisoidMove ThisPenis;
        private Transform cam;
        private Vector3 camforward;
        private Vector3 move=Vector3.zero;
        private bool jump = false;
        
        void Start()
        {

            // get the transform of the main camera
            if (Camera.main != null)
            {
                cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }


            ThisPenis = GetComponent<PenisoidMove>();  //init
        }


        void FixedUpdate()
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            jump = Input.GetKey(KeyCode.Space);


            // calculate move direction to pass to character
            if (cam != null)
            {
                // calculate camera relative direction to move:
                camforward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
                move = v * camforward + h * cam.right;
            }



            //pass move vector to move script
            ThisPenis.Move(move, jump);
            
        }
    }
}

