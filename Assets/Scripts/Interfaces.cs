﻿using UnityEngine;

public interface IDamagable
{
    void Damage(int dmg);
}

public interface IKillable
{
    void Deth();
}

public interface IHealable
{
    void Heal(int heal);
}

public interface IKnockBackable
{
    void Knockback(float power, Vector3 dir);
}