﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;




public class Unit : MonoBehaviour
{
    private int _hp;
    public int HP
    {
        get
        {
            return _hp;
        }

        protected set
        {
            _hp = value;
            //callback to broadcast HP change
            if (hpChanged != null)
            {
                hpChanged(_hp, _maxhp);
                Debug.Log("HP Changed");
            }
        }
    }
    private int _maxhp;
    public int MaxHP
    {
        get
        {
            return _maxhp;
        }

        protected set
        {
            _maxhp = value;
            //callback to broadcast MaxHP change
            if (hpChanged != null)
            {
                hpChanged(_hp, _maxhp);
                Debug.Log("MaxHP changed");
            }
        }
    }
    public float KnockBackForce = 500;
    public float KnockUpForce = 500;
    public bool isDed { get; protected set; }

    Action<int, int> hpChanged;
    protected Rigidbody rb;
    protected Animator anim;

    protected AudioSource asource;
    public AudioClip HitSound;


    public virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        asource = GetComponentInChildren<AudioSource>();
    }

    public void RegisterHPChanged(Action<int, int> callback)
    {
        hpChanged += callback;
        Debug.Log("Something has subscribed to hpChanged in "+gameObject.name);
    }

    public void unRegisterHPChanged(Action<int, int> callback)
    {
        hpChanged -= callback;
    }

    public virtual void Damage(int dmgAmount)
    {
        HP -= dmgAmount;
        asource.PlayOneShot(HitSound);
        Debug.Log("Unit Damaged");
        if (HP <= 0)
        {
            HP = 0;
            Deth();
        }
    }

    public virtual void Heal(int hlAmount)
    {
        if (HP < MaxHP)
        {
            HP += hlAmount;
            if (HP >= MaxHP)
            {
                _hp = _maxhp;
            }
        }
    }


    public void Kill()
    {
        HP = 0;
        Deth();
    }

    protected void Deth()
    {
        OnUnitDeth();
        Debug.Log("Unit " + gameObject.name + " has died.");
        //Destroy(gameObject);
    }

    virtual public void OnUnitDeth() {

    }


    /*******************************************
//the following is used only for knockback*/
    private bool kbing = false;
    protected int kbticks = 0;
    private Vector3 kbpoint;
    private Vector3 kbforce;

    public void KnockBack(Vector3 point)
    {
        if (!kbing)
        {
            //set up conditions for knockback next fixed tick
            kbpoint = point;
            kbforce = (gameObject.transform.position - kbpoint).normalized * KnockBackForce;
            kbforce.y = KnockUpForce;
            kbticks = 8;
            kbing = true;
        }
    }

    public virtual void FixedUpdate() //basically making sure we're running knockback in sync with physics
    {
        if (kbing)
        {
            if (kbticks > 0)
            {
                anim.applyRootMotion = false;
                rb.velocity = new Vector3(0, 0, 0);
                rb.AddForce(kbforce, ForceMode.Impulse);
                //Debug.Log("added force" + kbforce);
                kbticks--;
            }
            else
            {
                kbing = false;
               
            }
        }
    }


}

