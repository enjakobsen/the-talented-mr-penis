﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenisoidAudio : MonoBehaviour {

    private AudioSource asource;
    public AudioClip[] clips;

	// Use this for initialization
	void Start () {
        asource = GetComponentInChildren<AudioSource>();
	}
	
    public void Leftstep()
    {
        asource.PlayOneShot(clips[0],0.2f);
    }

    public void Rightstep()
    {
        asource.PlayOneShot(clips[1],0.2f);
    }

    public void Squirt()
    {
        asource.PlayOneShot(clips[2]);
    }
}
