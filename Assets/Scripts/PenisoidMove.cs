﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EmilsAssets
{
    public class PenisoidMove : MonoBehaviour
    {
        private Animator anim;
        private Rigidbody rb;
        private Vector3 GroundNormal = Vector3.up;
        private float turn;
        private float forward;
        private bool isGrounded;
        private bool isSlipping;
        private Vector3 old_forward;
        private bool isJumping;
        private float jumptimer;

        [SerializeField] float JumpPower = 2f;
        [SerializeField] float JumpTime = 1f;
        [SerializeField] float GroundCheckDistance = 0.2f;
        [SerializeField] float SlipAngle = 40f;
        [SerializeField] float GroundOriSpeed = 10f;
        [SerializeField] float TurnSpeed = 90f;
        [SerializeField] float GravityMultiplier = 2f;

        void Awake()
        {
            anim = GetComponent<Animator>(); //init
            rb = GetComponent<Rigidbody>();
        }

        public void Move(Vector3 move, bool jump)
        {
            GroundCheck(); //checks whether grounded and fetches ground normal

            if (isGrounded)
            {
                if (Vector3.Angle(Vector3.up, GroundNormal) > SlipAngle)
                {
                    isSlipping = true;
                }
                OrientToGround();

                move.Normalize(); //normalize move vector in case shit
                move = transform.InverseTransformDirection(move); //convert to local space
                                                                  //move = Vector3.ProjectOnPlane(move, GroundNormal); //project on ground, not needed when player already oriented to ground.
                turn = Mathf.Atan2(move.x, move.z); //in radians
                //turn = turn / Mathf.PI; //in domain [-1,1]   better leave this out to help show the turn animation
                turn = Mathf.Clamp(turn * 10f, -1f, 1f);
                //Debug.Log(turn);                   
                
                if (move.z > 0.5f)  //dirty conditional to max out the movement speed
                {
                    forward = 1;
                }
                else
                {
                    forward = 0;
                }
                TurnHelper();
                anim.SetFloat("Forward", forward, 0.1f, Time.deltaTime);
                anim.SetFloat("Turn", turn, 0.1f, Time.deltaTime);

                if (jump)
                {
                    rb.velocity = rb.velocity + (Vector3.up * JumpPower);
                    isJumping = true;
                }
                else if (isJumping && !jump)
                {
                    isJumping = false;
                    jumptimer = JumpTime;
                }
            }
            else //if airborne
            {
                Vector3 extraGravityForce = (Physics.gravity * GravityMultiplier) - Physics.gravity;
                rb.AddForce(extraGravityForce);
            }

            if (jump && isJumping && jumptimer>0f)   //shitty jump
            {
                rb.velocity = rb.velocity + (Vector3.up * JumpPower);
                jumptimer -= Time.fixedDeltaTime;
            }
             
            if (isGrounded==false && rb.velocity.y<0.01f) // help, I've fallen and I can't get up
            {
                OrientToGround();
            }

        }




        private void GroundCheck()
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position + (transform.up * 0.1f), -Vector3.up, out hit, GroundCheckDistance))
            {
                GroundNormal = hit.normal;
                isGrounded = true;
                anim.applyRootMotion = true;
            }
            else
            {
                isGrounded = false;
                GroundNormal = Vector3.up;
                anim.applyRootMotion = false;
            }

        }



        private void OrientToGround()
        {
            old_forward = transform.forward;
            transform.rotation=Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Vector3.Normalize(Vector3.ProjectOnPlane(old_forward, GroundNormal)), GroundNormal),GroundOriSpeed*Time.deltaTime);
        }



        private void TurnHelper()
        {
            transform.Rotate(0, turn * TurnSpeed * Time.deltaTime, 0);
        }
    }
}