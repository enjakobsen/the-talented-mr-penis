﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITargetIndicator : MonoBehaviour {

    private TargetManager tm;
    private RectTransform rt;
    public Renderer rend;
    private Vector3 screencenter;
    private Vector2 min, max;
    private float boxsize;
    public bool hastarget;
    [SerializeField]
    private float rotationspeed=100f;





	// Use this for initialization
	void Start () {
        tm = FindObjectOfType<TargetManager>();
        rt = GetComponentInChildren<RectTransform>();

    }
	
	// Update is called once per frame
	void Update () {
        if (tm.targetgo != null)
        {
            if (!hastarget)
            {
                Retarget();
            }
            if (rend != null)
            {
                screencenter = Camera.main.WorldToScreenPoint(rend.bounds.center);

                Vector3[] screencorners = new Vector3[8];

                screencorners[0] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x + rend.bounds.extents.x, rend.bounds.center.y + rend.bounds.extents.y, rend.bounds.center.z + rend.bounds.extents.z));
                screencorners[1] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x + rend.bounds.extents.x, rend.bounds.center.y + rend.bounds.extents.y, rend.bounds.center.z - rend.bounds.extents.z));
                screencorners[2] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x + rend.bounds.extents.x, rend.bounds.center.y - rend.bounds.extents.y, rend.bounds.center.z + rend.bounds.extents.z));
                screencorners[3] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x + rend.bounds.extents.x, rend.bounds.center.y - rend.bounds.extents.y, rend.bounds.center.z - rend.bounds.extents.z));
                screencorners[4] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x - rend.bounds.extents.x, rend.bounds.center.y + rend.bounds.extents.y, rend.bounds.center.z + rend.bounds.extents.z));
                screencorners[5] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x - rend.bounds.extents.x, rend.bounds.center.y + rend.bounds.extents.y, rend.bounds.center.z - rend.bounds.extents.z));
                screencorners[6] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x - rend.bounds.extents.x, rend.bounds.center.y - rend.bounds.extents.y, rend.bounds.center.z + rend.bounds.extents.z));
                screencorners[7] = Camera.main.WorldToScreenPoint(new Vector3(rend.bounds.center.x - rend.bounds.extents.x, rend.bounds.center.y - rend.bounds.extents.y, rend.bounds.center.z - rend.bounds.extents.z));



                min = screencorners[0];
                max = screencorners[0];


                foreach(Vector3 v in screencorners)
                {
                    min = Vector2.Min(min, v);
                    max = Vector2.Max(max, v);
                }


                //find one size to use, since we're rotating the damn thing now
                boxsize = Mathf.Max(max.x - min.x, max.y - min.y);

                transform.GetChild(0).gameObject.SetActive(true);
                rt.position = screencenter;
                rt.sizeDelta = new Vector2(boxsize, boxsize);
                rt.Rotate(0, 0, rotationspeed * Time.deltaTime);
            }

        }
        else
        {
            hastarget = false;
            transform.GetChild(0).gameObject.SetActive(false);
            
        }


        }

    private void Retarget()
    {
        rend = tm.targetgo.GetComponentInChildren<Renderer>();
        hastarget = true;
        //Debug.Log("getcomponent called");
    }
		
	}

