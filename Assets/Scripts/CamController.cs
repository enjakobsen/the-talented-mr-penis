﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour {

    public Transform CamPrimaryTarget;
    private TargetManager tm;
    private Vector3 WantedCamPosition;
    private Quaternion WantedCamRotation;
    private Quaternion CurrentCamRotation;
    private Quaternion WantedYRotation;
    private float WantedCamRotationAngle;

    [SerializeField]
    private float Height=5f;
    [SerializeField]
    private float Distance=10f;
    [SerializeField]
    private float Damping = 1f;


    void Start()
    {
        tm = FindObjectOfType<TargetManager>();
    }


    // Update is called once per frame
    void LateUpdate () {

        

        if (tm.targetgo == null) //if no target is selected
        {
            WantedCamRotationAngle = CamPrimaryTarget.eulerAngles.y; // prefer to be behind player


            //calculate new position, rotation
            WantedYRotation = Quaternion.Euler(0, WantedCamRotationAngle, 0);
            WantedCamRotation = Quaternion.LookRotation(CamPrimaryTarget.position - transform.position);
            WantedCamPosition = CamPrimaryTarget.position - WantedYRotation * Vector3.forward * Distance + Vector3.up * Height;





        }
        else if (tm.targetgo != null) //target mode
        {
            Vector3 temp = new Vector3(CamPrimaryTarget.transform.position.x-tm.targetgo.transform.position.x,0f, CamPrimaryTarget.transform.position.z - tm.targetgo.transform.position.z);

            WantedCamRotationAngle = Vector3.Angle(temp.normalized, Vector3.forward); //in domain [0,180]
            if (Vector3.Cross(temp.normalized, Vector3.forward).y > 0) //if angle is below temp
            {
                WantedCamRotationAngle = -WantedCamRotationAngle;  //flip to domain [-180, 180]
            }

            WantedCamRotationAngle += 180f; //in domain [0,360]



            WantedYRotation = Quaternion.Euler(0, WantedCamRotationAngle, 0);
            WantedCamRotation = Quaternion.LookRotation(tm.targetgo.transform.position-CamPrimaryTarget.position);
            WantedCamPosition = CamPrimaryTarget.position - WantedYRotation * Vector3.forward * Distance + Vector3.up * Height;
        }


        transform.position = Vector3.Lerp(transform.position, WantedCamPosition, Damping * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, WantedCamRotation, Damping * Time.deltaTime);





    }
}

