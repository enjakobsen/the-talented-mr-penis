﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WSpaceHealthbar : MonoBehaviour {

    Unit myUnit;
    RectTransform rt;

	// Use this for initialization
	void Start () {
        myUnit = GetComponentInParent<Unit>();
        rt = GetComponentsInChildren<RectTransform>()[1];
	}
	
	// Update is called once per frame
	void Update () {
            transform.LookAt(Camera.main.transform.position);
            if (myUnit.MaxHP > 0)
            {
                rt.sizeDelta = new Vector2(((float)myUnit.HP / (float)myUnit.MaxHP), 1);
            }
    }
}
